import { Component, OnInit } from '@angular/core';
import { Caracter } from './model/caracter';

declare var $: any;

@Component({
  selector: 'app-caracter',
  templateUrl: './caracter.component.html',
  styleUrls: ['./caracter.component.scss'],
})
export class CaracterComponent implements OnInit {
  dataCharacter = ['Talla', 'Color', 'Garantia', 'Marca', 'Material'];
  characteristicData: Caracter[] = [];

  constructor() {}

  buttonAddTable(): void {
    this.addCharacteristic($('#caracters').val());
  }

  addCharacteristic(charactName: string): void {
    let characteristicNewData: Caracter = { name: charactName, description: [] };
    this.characteristicData.push(characteristicNewData);
  }

  /* methodSubject(characterBox, characterName): void {
    for (const characteristic of this.characteristicData) {
      if (characterName.toString() != characteristic.name.toString()){
        this.characteristicData.push(characterBox);
        break;
      }
    }
  } */

  viewArray(): void {
    console.log(this.characteristicData);
  }

  actionDescription(event, index): void{
    let validations = event.target.value;
    if (validations != ''){
      for (let i = 0; i <= this.characteristicData.length; i++){
        if (i == index){
          this.characteristicData[i].description.push(validations);
          break;
        }
      }
    }
    event.target.value = '';
  }

  deleteDescription(index, sector): void{
    for (let i = 0; i <= this.characteristicData.length; i++){
      if (i == index){
        this.characteristicData[i].description.splice(sector, 1);
        break;
      }
    }
  }

  deleteCharacter(index): void {
    this.characteristicData.splice(index, 1);
    console.log(this.characteristicData);
  }

  ngOnInit(): void {}
}
